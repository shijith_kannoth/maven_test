package com.auto.bdt.pageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.auto.bdt.helpers.Log;
public class AutomationHomePage extends BaseClass{

	public AutomationHomePage(WebDriver driver){
		super(driver);
	}    

	
	@FindBy(how=How.LINK_TEXT, using="Sign in")
	public static WebElement sign_in;
	
	@FindBy(how=How.LINK_TEXT, using="Contact us")
	public static WebElement contact_us;
	
	@FindBy(how=How.LINK_TEXT, using="Sign out")
	public static WebElement sign_out;
		
	public static class HeaderPage 
	{
		
		@FindBy(how=How.LINK_TEXT, using="whispir")
		public static WebElement menu_whipir;
		
		@FindBy(how=How.XPATH, using="//*a[@title='panel']")
		public static WebElement menu_panel;
		
		@FindBy(how=How.XPATH, using="//*a[@title='frame']")
		public static WebElement menu_frame;
		
		
		public static class DressesPage
		{
				
			@FindBy(how=How.XPATH, using="//*a[@title='whispir objects']")
			public static WebElement whispir_objects;
			
		}
		
	}
	
}
		

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	