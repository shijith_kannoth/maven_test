package com.auto.bdt.stepDefintions;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.auto.bdt.stepDefintions.Hooks;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class LoginStepDefs{
    public WebDriver driver;
    
    public LoginStepDefs()
    {
    	driver =  com.auto.bdt.stepDefintions.Hooks.driver;
    }
    

@Given("^I open the url$")
public void i_open_the_url() throws Throwable {
    driver.get("https://app.whispir.it");
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.manage().window().maximize();
}

@When("^I input my user name and password$")
public void i_input_my_user_name_and_password() throws Throwable {
	driver.findElement(By.xpath("//input[@id='id-username']")).sendKeys("suresht");

     System.out.println("User name entered 1111111111");;
	// this is for pwd
	driver.findElement(By.xpath("//input[@id='id-password']")).sendKeys("Suresh1122");
	System.out.println("PASSWORD entered 2222222222222");;
	// this is for login button
	driver.findElement(By.xpath("//section[@id='whp-login-card']/form/button")).click();

//	Log.info("User Logged in User");
	System.out.println("LOGGED IN SUCCEFULLY 33333333333");;
	Thread.sleep(1000);
}

@Then("^I should go to the home page$")
public void i_should_go_to_the_home_page() throws Throwable {
//	String text=driver.findElement(By.xpath("//div[@id='mainContent']/form[4]/h1")).getText();
//	assertEquals("View My Messages", text);
	System.out.println("Welcome to Home Page 444444444444");
	//Log.info("Welcome to Home page");
}
}