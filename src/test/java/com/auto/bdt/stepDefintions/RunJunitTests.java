package com.auto.bdt.stepDefintions;

import junit.framework.TestCase;

import java.awt.List;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
//import cucumber.runtime.PendingException;

public class RunJunitTests {
public WebDriver driver;	
public String baseUrl;
  
  @Before
  public void setUp()
  {
	    //driver = new FirefoxDriver();
//	    System.setProperty("webdriver.chrome.driver", "E://chromedriver.exe");
	    driver = new ChromeDriver(); 
	    baseUrl = "http://au.whispir.com/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
  }

  @Given("Invoke the browser and login using \"([^\"]*)\" and \"([^\"]*)\"")
  public void login(String arg1, String arg2)
  {
	  try
	  {
	    driver.get(baseUrl + "/");
	    driver.manage().window().maximize();
	    driver.findElement(By.id("id-username")).sendKeys(arg1);
	    driver.findElement(By.id("id-password")).sendKeys(arg2);
	    driver.findElement(By.xpath("//button[@type='button']")).click();
	  }
	  catch(Exception e){
		e.printStackTrace();
	  }
  }
  
  
  @Then("Navigate to the new message creation Page")
  public void new_message()
  {
	  try
	  {
	    Thread.sleep(30000);
	    driver.get("https://au.whispir.com/tmpl/home.tmpl");
//	    driver.get("https://au.whispir.com/tmpl/home.tmpl");
//	    driver.get("https://au.whispir.com/tmpl/home.tmpl");
//	    driver.get("https://app.whispir.it/tmpl/home.tmpl");
//	    driver.get("https://app.whispir.it/tmpl/home.tmpl");
	    driver.findElement(By.xpath("/html/body")).sendKeys(Keys.F5);
	    
	    
	    Thread.sleep(5000);
	    Actions action = new Actions(driver);
	    WebElement we = driver.findElement(By.xpath("//*[@id='menu']/ul/li[1]/div/h2"));
	    action.moveToElement(we).build().perform();
	    Thread.sleep(5000);
	    driver.findElement(By.xpath("(//a[contains(text(),'New Message')])[2]")).click();
	  }
	  catch(Exception e)
	  {
		 e.printStackTrace();
	  }
  }
  
//  @Then("^Enter all the required data like \"(.*?)\" \"(.*?)\" and \"(.*?)\" and \"(.*?)\" and \"(.*?)\"$")
//  @Then("^Enter all the required data like \"(.*?)\" \"(.*?)\" and \"(.*?)\" and \"(.*?)\" and \"(.*?)\"$")
//  @Then("^Enter all the required data like \"(.*?)\" \"(.*?)\" and \"(.*?)\" and \"(.*?)\" and \"(.*?)\"$")
//        enter_all_the_required_data_like_and_and_and(String arg1, String arg2, String arg3, String arg4, String arg5) throws Throwable
//  @And("Enter all the required data like \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"")
@Then("^Enter all the required data like \"(.*?)\" and \"(.*?)\" and \"(.*?)\" and \"(.*?)\" and \"(.*?)\"$")
//@Then("^Enter all the required data like \"(.*?)\" \"(.*?)\" and \"(.*?)\" and \"(.*?)\" and \"(.*?)\"$")
//  public void enter_all_the_required_data_like_and_and_and(String arg1, String arg2, String arg3, String arg4, String arg5) throws Throwable 
  public void entermessagedata(String c, String email, String s, String n, String b)
  {
	  try
	  {
		  driver.findElement(By.id("toList")).clear();
		  driver.findElement(By.id("toList")).sendKeys(c);
		  
//		  driver.findElement(By.id("toList")).clear();
		  driver.findElement(By.id("toList")).sendKeys(email);

		  
		  driver.findElement(By.id("toggleCcBccButton")).click();
		  driver.findElement(By.id("Subject")).clear();
		  driver.findElement(By.id("Subject")).sendKeys(s);
		  driver.findElement(By.id("mobileTab_caption")).click();
		  driver.findElement(By.id("NickName")).clear();
		  driver.findElement(By.id("NickName")).sendKeys(n);
		  driver.switchTo().frame("iframeBody");
		  WebElement element = driver.findElement(By.xpath("//*[@id='editableContent']"));
		  element.sendKeys(b);
		  driver.switchTo().defaultContent();  
	  }
	  catch(Exception e){
		  System.out.println(e);
	  }
  }
  
  @And("Enter text \"([^\"]*)\" in the Email body and change to rich text mode")
  public void emailtext(String email)
  {
	  try{
		  driver.findElement(By.xpath("//*[@id='emailTab_caption']")).click();
		  
		  
		  driver.switchTo().frame("iframeEmailBody");
		  WebElement element = driver.findElement(By.xpath("//*[@id='editableContent']"));
		  element.sendKeys(email);
		  driver.switchTo().defaultContent(); 
		  
		  driver.findElement(By.xpath("//*[@id='emailBodyEditorMenutoggleBtn']")).click();
	  }
	  catch(Exception e)
	  {
		  e.printStackTrace();
		  
	  }
  }
  
  @Then("Click on send button and verify the status of the message")
  public void sendandverify()
  {
	  try{
		  driver.findElement(By.cssSelector("div.whp-form-actions.whp-comp-btn > button.whp-btn.whp-large")).click();
		  Thread.sleep(5000);
		 
		  driver.findElement(By.xpath("//*[@id='inbox']/tbody/tr[1]/td[7]/a")).click();
		  driver.findElement(By.id("reloadMessageStatus")).click();
		  driver.findElement(By.id("messageResponseTab")).click();
		  driver.findElement(By.id("publishingStatusTab")).click();  
	  }
	  catch(Exception e)
	  {
		  System.out.println(e);
		  e.printStackTrace();
	  }
  }
  
//  @Given("^I open the url$")
//  public void i_open_the_url() throws Throwable {
//      driver.get("https://au.whispir.com");
//      driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//  	driver.manage().window().maximize();
//  }

  
  @Then("Navigate to the contact reports Page")
  public void contactreports()
  {
	  try{
		    Thread.sleep(30000);
		    driver.get("https://au.whispir.com/tmpl/home.tmpl");
		    driver.get("https://au.whispir.com/tmpl/home.tmpl");
		    driver.get("https://au.whispir.com/tmpl/home.tmpl");
		    driver.findElement(By.xpath("/html/body")).sendKeys(Keys.F5);
		    
		    
		    Thread.sleep(5000);
		    Actions action = new Actions(driver);
		    WebElement we = driver.findElement(By.xpath("//*[@id='menu']/ul/li[1]/div/h2"));
		    action.moveToElement(we).build().perform();
		    Thread.sleep(5000);
		    driver.findElement(By.linkText("Contact Reports")).click();
	  }
	  catch(Exception e)
	  {
		  e.printStackTrace();
	  }
  }
  
  @And("Select the report type and add the required field")
  public void selectreport()
  {
	  try
	  {
		  Select droplist = new Select(driver.findElement(By.id("selectedReport")));   
		  droplist.selectByVisibleText("Contacts & Users within Distribution Lists - Sort by List Name");
		 //new Select(driver.findElement(By.id("selectedReport"))).selectByVisibleText("Contacts & Users within Distribution Lists - Sort by List Name");
	     driver.findElement(By.cssSelector("option[value=\"r1\"]")).click();

		 WebElement elem = driver.findElement(By.id("availableFields"));
		 new Select(elem).selectByVisibleText("Dist List");
		 driver.findElement(By.id("addToListButton")).click();
		 WebElement elem1 = driver.findElement(By.id("availableFields"));
		 new Select(elem1).selectByVisibleText("First Name");
		 driver.findElement(By.id("addToListButton")).click();
		 WebElement elem2 = driver.findElement(By.id("availableFields"));
		 new Select(elem2).selectByVisibleText("Last Name");
		 driver.findElement(By.id("addToListButton")).click();
		 WebElement elem3= driver.findElement(By.id("availableFields"));
		 new Select(elem3).selectByVisibleText("Email");
		 driver.findElement(By.id("addToListButton")).click();
		 WebElement elem4 = driver.findElement(By.id("availableFields"));
		 new Select(elem4).selectByVisibleText("Mobile");
		 driver.findElement(By.id("addToListButton")).click();
	  }
	  catch(Exception e)
	  {
		  e.printStackTrace();
		  
	  }
  }
  
  @Then("Click on export button to export reports to local drive")
  public void exortclick()
  {
	  try{
		  driver.findElement(By.cssSelector("button.whp-btn.whp-large")).click();
		  Thread.sleep(5000);
	  }
	  catch(Exception e)
	  {
		  e.printStackTrace();
		  
	  }
  }
  
  @Then("Close the browser and exit")
  public void logoutandclose()
  {
	  driver.quit();
  }
  
  
  @After
  public void tearDown()
  {
	  //driver.quit();
  }
 
    
  }

 

