package com.auto.bdt.stepDefintions;

import java.net.MalformedURLException;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks{
    public static WebDriver driver;

    
    @Before
    /**
     * Delete all cookies at the start of each scenario to avoid
     * shared state between tests
     */
    public void openBrowser() throws MalformedURLException {
    	System.out.println("Called openBrowser 12345");
        System.setProperty("webdriver.chrome.driver","C:\\Jar\\browser\\chromedriver.exe");
    	driver = new ChromeDriver();
       //  System.setProperty("webdriver.firefox.driver","C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
    	//driver = new FirefoxDriver();
//    	System.setProperty("webdriver.chrome.driver", "F:\\LIB\\chromedriver.exe");
     	driver.manage().deleteAllCookies();
    	driver.manage().window().maximize();
    }

     
    @After
    /**
     * Embed a screenshot in test report if test is marked as failed
     */
    public void embedScreenshot(Scenario scenario) {
       
        if(scenario.isFailed()) {
        try {
        	 scenario.write("Current Page URL is " + driver.getCurrentUrl());
//            byte[] screenshot = getScreenshotAs(OutputType.BYTES);
//        	 File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//            byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);  //-ORIGINAL
            
            byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
            
            //-- ADDING BELOW FOR EXPERIEMENTING-----
            DateFormat dateFormat = new SimpleDateFormat("dd_MMM_yyyy__hh_mm_ssaa");
    		String destDir = "target/surefire-reports/screenshots";
            
    		new File(destDir).mkdirs();
    		String destFile = dateFormat.format(new Date()) + ".png";
    		
//    		FileUtils.copyFile(screenshot, new File(destDir + "/" + destFile));
            //-- ADDING ABOVE FOR EXPERIEMENTING-----
        } catch (WebDriverException somePlatformsDontSupportScreenshots) {
            System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            
            somePlatformsDontSupportScreenshots.printStackTrace();
        }
        
        }
        driver.quit();
        
    }
    
}