package com.auto.bdt.stepDefintions;

import static org.testng.AssertJUnit.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.auto.bdt.helpers.DataHelper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class NewMessageStepDefs{
    public WebDriver driver;
    
    public NewMessageStepDefs()
    {
    	driver = Hooks.driver;
    }
    

@When("^Click on new message$")
public void click_on_new_message() throws Throwable {
	driver.findElement(By.xpath("html/body/div[2]/div/header/nav/ul/li[1]")).click();
	System.out.println("Clicked on My Company");
	Thread.sleep(6000);
    driver.findElement(By.xpath("html/body/div[2]/div/header/nav/ul/li[1]/ul/li/div[1]/div[2]/ul[1]/li[2]/a")).click();
    System.out.println("Clicked on Enter New Message");
    Thread.sleep(6000);
    
}

@When("^enter details$")
public void enter_details() throws Throwable {
	
	/*driver.findElement(By.xpath("//input[@id='toList']")).sendKeys("918885252461");
    driver.findElement(By.xpath("//input[@id='tagsElem']")).sendKeys("testing");
    driver.findElement(By.xpath("//input[@id='Subject']")).sendKeys("testMessage");
    
    //driver.switchTo().frame("iframeHtmlCommonBody");
    driver.switchTo().frame(0);
    
    driver.findElement(By.xpath("//body[@id='editableContent']")).sendKeys("Test Discription");
    //driver.switchTo().window(null);
    driver.switchTo().defaultContent();
    driver.findElement(By.xpath("//*[@id='web_com_0200']/div[12]/button[1]")).click();;
    Thread.sleep(6000);*/
	DataHelper dh=new DataHelper();
	System.out.println("Phone number "+dh.testData());
	List<String> list=dh.testData();
	System.out.println("Size@@"+list.size());
	/*System.out.println("label number "+dh.data().get(1));
	System.out.println("subject number "+dh.data().get(2));
	System.out.println("Discription number "+dh.data().get(3));*/
	driver.findElement(By.xpath("//input[@id='toList']")).sendKeys(list.get(0));
    driver.findElement(By.xpath("//input[@id='tagsElem']")).sendKeys(list.get(1));
    driver.findElement(By.xpath("//input[@id='Subject']")).sendKeys(list.get(2));
//    driver.findElement(By.xpath("//Subject]")).sendKeys(list.get(2));
    
    //driver.switchTo().frame("iframeHtmlCommonBody");
    driver.switchTo().frame(0);
    
    driver.findElement(By.xpath("//body[@id='editableContent']")).sendKeys(list.get(3));
    //driver.findElement(By.xpath("//body[@id='editableContent']")).sendKeys("Test Discription");
    //driver.switchTo().window(null);
    driver.switchTo().defaultContent();
    driver.findElement(By.xpath("//*[@id='web_com_0200']/div[12]/button[1]")).click();;
    Thread.sleep(6000);
}

@Then("^validate result$")
public void validate_result() throws Throwable {
    
}


    
}