package com.auto.bdt.stepDefintions;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TabImplementStepDefs {
	
	@Given("^user navigates to http://app(\\d+)web\\.dev(\\d+)\\.whispir\\.net/$")
	public void user_navigates_to_http_app_web_dev_whispir_net(int arg1, int arg2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("^login with valid credentials$")
	public void login_with_valid_credentials() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^login should be successful$")
	public void login_should_be_successful() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^click on Admin tab on right top\\.$")
	public void click_on_Admin_tab_on_right_top() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^click on Company Settings$")
	public void click_on_Company_Settings() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^verify Auto - Active Contact tab implemented\\.$")
	public void verify_Auto_Active_Contact_tab_implemented() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^Select workspaces$")
	public void select_workspaces() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^click save$")
	public void click_save() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}


}
